<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/config.php';

if(session_status() === PHP_SESSION_NONE) {
  session_start();
}



$claim_url = $cfg_site_url . '/faucet.php?';

$first = true;
foreach ($_POST as $key => $value) {
  if($key=='address' || $key=='currency' || $key=='refresh_time'){
  if ($first) {
    $claim_url .= rawurlencode($key) . '=' . rawurlencode($value);
    $first = false;
  } else {
    $claim_url .= '&' . rawurlencode($key) . '=' . rawurlencode($value);
  }
}
}

unset($key);
unset($value);
unset($first);

if ($cfg_use_captcha) {
  require_once $_SERVER['DOCUMENT_ROOT'] . '/lib/captcha.php';

  if (!verify_captcha($cfg_captcha_type)) {
    http_response_code(400);
    die('Failed to verify CAPTCHA.');
  } 
}

if (isset($_GET["r"])){
  if ($_POST["address"] == $_GET["r"]){
    die("You seem to have tried to cheat the system by double-claiming.");
  }
}


if ($cfg_enable_proxycheck) {
  $user_ip = get_user_ip();
  
  require_once $_SERVER['DOCUMENT_ROOT'] . '/lib/proxycheck.php';
  if (check_proxycheck($user_ip)) {
    header('Location: ' . $cfg_site_url . '/proxycheck.php', true, 302);
    exit;
  }
}

function generateRandomString($length = 15) {
  return substr(str_shuffle(str_repeat($x='abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz012345678901234567890123456789', ceil($length/strlen($x)) )),1,$length);
}

$string = generateRandomString($length = 15);

$key = rawurlencode(md5($_POST['address'] . ' '.$string.' ' . $cfg_cookie_key));
$currency = $_POST['currency'];

if (!isset($_SESSION["key_".$currency.""])){
  $_SESSION["key_".$currency.""] = $key;
}else{
  die("There already is a Session for ".$currency."");
}

$claim_url .= '&key=' . $key;

if ($cfg_use_shortlink) {
  require_once $_SERVER['DOCUMENT_ROOT'] . '/lib/shortlink.php';

  $claim_url = shortlink_create($claim_url);
}

header('Location: ' . $claim_url, true, 303);
exit;
?>
