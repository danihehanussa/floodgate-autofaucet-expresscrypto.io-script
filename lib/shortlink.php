<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/config.php';


/*

Our Updated Faucet-Friendly Shortlinks List here :

https://dutchycorp.space/faucetlists/shortlinks-list

*/

function shortlink_create($longurl) {


  $l[1] = 'https://dutchycorp.space/sh/api?api=1e07190c0bf8fdb61ee56e221f19093ae5b6133e&url=';
  $l[2] = 'https://dutchycorp.space/sh/api?api=1e07190c0bf8fdb61ee56e221f19093ae5b6133e&url=';
  $l[3] = 'https://dutchycorp.space/sh/api?api=1e07190c0bf8fdb61ee56e221f19093ae5b6133e&url=';
  $l[4] = 'https://dutchycorp.space/sh/api?api=1e07190c0bf8fdb61ee56e221f19093ae5b6133e&url=';
  $l[5] = 'https://dutchycorp.space/sh/api?api=1e07190c0bf8fdb61ee56e221f19093ae5b6133e&url=';
  $l[6] = 'https://dutchycorp.space/sh/api?api=1e07190c0bf8fdb61ee56e221f19093ae5b6133e&url=';
  $l[7] = 'https://bit-url.com/api?api=ff18eee8542361cb7505e866f3728e7a559ef64f&url=';
  $l[8] = 'https://dutchycorp.space/sh/api?api=1e07190c0bf8fdb61ee56e221f19093ae5b6133e&url=';
  $l[9] = 'https://dutchycorp.space/sh/api?api=1e07190c0bf8fdb61ee56e221f19093ae5b6133e&url=';
  
  
  

  $p = 1;
  $size_of_list = count($l);
  if(isset($_COOKIE[$cfg_shortener_cookie_name]) && (int)$_COOKIE[$cfg_shortener_cookie_name] < $size_of_list){
    $p = (int)$_COOKIE[$cfg_shortener_cookie_name] + 1;
  } 
  setcookie($cfg_shortener_cookie_name, $p, time() + (86400 * 30), "/"); // 86400 = 1 day
  $result =  @json_decode(file_get_contents($l[$p] . urlencode($longurl)), true);
  if($result['status'] === 'error')
    die($result['message']);
  else
    return $result['shortenedUrl'];
  }
  
?>
