<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/config.php';

// returns true if iphub suggests blocking the address
function check_proxycheck(string $ip) {
  global $cfg_proxycheck_key;
  global $cfg_proxycheck_whitelist;

  if (isset($cfg_proxycheck_whitelist[$ip]))
    return false;


  $response = json_decode(file_get_contents('http://proxycheck.io/v2/' . $ip . '?key=' . $cfg_proxycheck_key . '', false));
 
  //print_r ($response->$ip->proxy);
  if ($response->status == "ok" && $response->$ip->proxy == "yes"){
    
    return true;
  }
  
}



?>
